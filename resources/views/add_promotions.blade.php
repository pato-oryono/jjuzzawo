@extends('layouts.master')

@section('content')
    <div>
        <h3>Add a promotion</h3>
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @elseif(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        <div>
            <div class="com-md-6">
                <form action="/promotions/add" method="post">
                    <input value="{{ csrf_token() }}" name="_token" type="hidden"/>
                    <div class="form-group"><input class="form-control" type="text" placeholder="Name of Promotion" name="promotion_name"></div>
                    <div class="form-group"><input class="form-control" type="hidden" name="station_id"></div>
                    <div class="form-group"><textarea class="form-control" name="description" placeholder="description"></textarea></div>
                    <div class="form-group"><input class="form-control" type="date" name="start_date" placeholder="Start Date"></div>
                    <div class="form-group"><input class="form-control" type="date" name="end_date" placeholder="End Date"></div>
                    <button type="submit" class="btn btn-success">Submit</button>

                </form>
            </div>
            <div class="col-md-6"></div>

        </div>
    </div>
@endsection