<div class="navbar navbar-default {{--navbar-fixed-top--}}" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><span class="fui-location"></span>  Jjuzzawo</a>

        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="/stations">Stations</a></li>
                <li><a href="/download">Download</a></li>
                <li><a href="/update">Update</a></li>
                <li><a href="/faqs">FAQs</a></li>
                <li><a href="/about">About Us</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if(Auth::guest())
                    <li><a href="{{ url('/auth/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fui-user"></span> {{ Auth::getUser()->fullName() }} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            {{--                            @if(Auth::getUser()->type() == 'Admin')
                                                            <li><a href="/dashboard">Dashboard</a></li>
                                                            <li><a href="/admin">Users</a></li>
                                                            <li><a href="/doc-types">Document types</a></li>
                                                            <li><a href="/documents">Documents</a></li>
                                                        @endif--}}
                            {{--<li><a href="/change_password">Change Password</a></li>
                            <li class="divider"></li>--}}
                            <li><a href="{{ url('/auth/login') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>


