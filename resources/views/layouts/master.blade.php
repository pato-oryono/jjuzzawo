<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flat-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
</head>
<body>

@include('partials.header')

<div class="container-fluid">
    @yield('content')
</div>

<div class="container">
    @yield('about')
</div>



<script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}"></script>

<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyD9uiyFbXfbSOsY6umTUtl4_rvl_LkQKxE&sensor=false&libraries=places'></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>



</body>
</html>