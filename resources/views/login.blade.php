@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">

            <h3>Login to Jjuzzawo.</h3>
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            @elseif(Session::has('message'))
                <div class="alert alert-warning">
                    {{ Session::get('message') }}
                </div>
            @endif
            <hr>
            <form action="" method="post">
                <input value="{{ csrf_token() }}" name="_token" type="hidden"/>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password">
                </div>

                <button class="btn btn-success" type="submit" name="submit">Login</button>
            </form>
        </div>
        <div class="col-md-4">

        </div>
    </div>

@endsection