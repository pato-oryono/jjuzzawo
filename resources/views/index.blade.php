@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div>
            <div>
                @foreach($stations as $station)
                    <h3>{{ $station->brand_name }}</h3>
                    <div>{{ $station->location }}</div>
                    <hr>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Fuel Type</th>
                                        <th>Price</th>
                                        <th></th>
                                    </tr>
                                </thead>

                                <tbody>
                                @if($prices->count())
                                    @foreach($prices as $price)
                                        <tr>
                                            <td>{{ $price->fuelType->name }}</td>
                                            <td>{{ $price->price }}</td>
                                            <td><a href="/">Edit</a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <td colspan="3"><div style="text-align: center; padding: 3em; font-size: 3em; color: #ccc;">
                                            <div><span></span></div>Nothing Found</div>
                                    </td>
                                @endif
                                </tbody>

                            </table>
                        </div>
                    @endforeach
            </div>
        </div>

    </div>

@endsection