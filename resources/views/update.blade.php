@extends('layouts.master')

@section('content')

    <div class="container-fluid">
        <div>
            <div>
                @foreach($stations as $station)
                    <h3>{{ $station->brand_name }}</h3>
                    <div>{{ $station->location }}</div>
                    <hr>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Fuel Type</th>
                                <th>Price</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>


                            @if($prices->count())

                                @foreach($prices as $price)
                                    <form method="post" action="/update/{{ $price->fuel_id }}">
                                    <tr>
                                        <td>{{ $price->fuelType->name }}</td>
                                        <td>
                                            <input value="{{ csrf_token() }}" name="_token" type="hidden"/>
                                            <input autofocus="1" style="border: none; padding: 5px; cursor: default" type="number" value="{{ $price->price }}" name="{{ $price->FuelType->name }}">
                                        </td>
                                        <td><button class="btn" type="submit">Update</button></td>
                                    </tr>
                                    </form>
                                @endforeach



                            @else
                                <td colspan="3"><div style="text-align: center; padding: 3em; font-size: 3em; color: #ccc;">
                                        <div><span></span></div>Nothing Found</div>
                                </td>
                            @endif
                            </tbody>



                        </table>
                    </div>
                @endforeach
            </div>
            <div>
                <h3>Services<span class="pull-right"><a href="{{ url('/services/add') }}">Add Service</a></span></h3>
                <div>

                    <table class="table table-striped">

                        @if(count($services))

                        <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>{{ $service->name }}</td>
                                <td>{{ $service->description }}</td>
                                <td><a href="/services/{{$service->id}}/delete"><span class="fui-trash"></span></a></td>

                            </tr>
                        @endforeach

                        </tbody>
                        @else
                            <td colspan="3"><div style="text-align: center; padding: 3em; font-size: 3em; color: #ccc;">
                                    <div><span></span></div>Nothing Found</div>
                            </td>
                        @endif



                    </table>

                </div>
            </div>

            <div>
                <h3>Promotions<span class="pull-right"><a href="{{ url('/promotions/add') }}">Add Promotion</a></span></h3>
                <div>
                    <table class="table table-striped">

                        @if(count($promotions))

                            <tbody>
                            @foreach($promotions as $promotion)
                                    <tr>
                                        <td>{{ $promotion->name }}</td>
                                        <td>{{ $promotion->description }}</td>
                                        <td><a href="/promotions/{{$promotion->id}}/delete"><span class="fui-trash"></span></a></td>
                                    </tr>
                            @endforeach

                            </tbody>
                        @else
                            <td colspan="3"><div style="text-align: center; padding: 3em; font-size: 3em; color: #ccc;">
                                    <div><span></span></div>Nothing Found</div>
                            </td>
                        @endif


                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection
