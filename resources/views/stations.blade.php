@extends('layouts.master')
@section('content')
    <div id="map-canvas" style="max-width: 100%; min-height: 730px; border: solid 1px #ccc; margin: 0"></div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9uiyFbXfbSOsY6umTUtl4_rvl_LkQKxE&sensor=false&libraries=places"></script>

    <script type="text/javascript">

        // https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false
        // //code.jquery.com/jquery-1.11.0.min.js

        var map;

        // The JSON data
        var json = [{"id":1,"brand_name":"Olympus","location":"Rubaga Rd, Kampala","tags":"","logo":"logos\/don.jpg","lat":"0.3644283","lng":"32.5445848","user_id":"1","created_at":"2016-04-17 10:50:08","updated_at":"2016-04-17 10:50:08"},{"id":2,"brand_name":"Mogas","location":"Kampala Road, Entebbe","tags":"","logo":"logos\/mogas.png","lat":"0.0750421","lng":"32.4660925","user_id":"2","created_at":"2016-04-17 10:50:08","updated_at":"2016-04-17 10:50:08"},{"id":3,"brand_name":"Kobil","location":"Kampala - Entebbe Rd","tags":"","logo":"logos\/don.jpg","lat":"0.0750421","lng":"32.4660925","user_id":"3","created_at":"2016-04-17 10:50:08","updated_at":"2016-04-17 10:50:08"},{"id":4,"brand_name":"Gapco","location":"Kiwafu, Entebbe","tags":"","logo":"logos\/gapco.png","lat":"0.0750421","lng":"32.4660925","user_id":"4","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":5,"brand_name":"Gaz","location":"Kampala - Entebbe Rd, Kajjansi","tags":"","logo":"logos\/gaz.png","lat":"0.2079029","lng":"32.47094","user_id":"5","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":6,"brand_name":"Hared","location":"Kampala","tags":"","logo":"logos\/don.png","lat":"0.1633751","lng":"32.4835967","user_id":"6","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":7,"brand_name":"Moil","location":"Kampala - Entebbe Rd, Kisubi","tags":"","logo":"logos\/don.jpg","lat":"0.1551655","lng":"32.4650474","user_id":"7","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":8,"brand_name":"Tan Oil","location":"Entebe","tags":"","logo":"logos\/don.jpg","lat":"0.06063","lng":"32.4137306","user_id":"8","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":9,"brand_name":"Shell","location":"Entebe","tags":"","logo":"logos\/shell.png","lat":"0.0554788","lng":"32.4031261","user_id":"9","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":10,"brand_name":"Shell","location":"Airport Road, Entebbe","tags":"","logo":"logos\/shell.png","lat":"0.0562933","lng":"32.4066991","user_id":"10","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":11,"brand_name":"Total","location":"Obote Avenue, Lira","tags":"","logo":"logos\/total.png","lat":"2.123717","lng":"32.5784693","user_id":"11","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":12,"brand_name":"Shell","location":"Lira","tags":"","logo":"logos\/shell.png","lat":"2.1243905","lng":"32.577782","user_id":"12","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":13,"brand_name":"Gapco","location":"Lira - Gulu Rd","tags":"","logo":"logos\/gapco.png","lat":"2.1738788","lng":"32.742451","user_id":"13","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":14,"brand_name":"Petrol","location":"Lira","tags":"","logo":"logos\/don.jpg","lat":"2.1250641","lng":"32.5770947","user_id":"14","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":15,"brand_name":"Petrol","location":"Lira","tags":"","logo":"logos\/don.jpg","lat":"2.1257376","lng":"32.5764074","user_id":"15","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":16,"brand_name":"Shell","location":"Lira","tags":"","logo":"logos\/shell.png","lat":"2.1264111","lng":"32.5757201","user_id":"16","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":17,"brand_name":"Gapco","location":"Lira - Gulu Rd, Lira","tags":"","logo":"logos\/gapco.png","lat":"2.1270846","lng":"32.5750328","user_id":"17","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":18,"brand_name":"Golden gas","location":"Juba Road, Lira","tags":"","logo":"logos\/don.jpg","lat":"2.1257001","lng":"32.5791549","user_id":"18","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":19,"brand_name":"Rhino oils","location":"Soroti Road, Lira","tags":"","logo":"logos\/don.jpg","lat":"2.1284317","lng":"32.5736582","user_id":"19","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":20,"brand_name":"Shell","location":"Lira - Gulu Rd, Lira","tags":"","logo":"logos\/shell.png","lat":"2.1291052","lng":"32.5729709","user_id":"20","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":21,"brand_name":"Petrocity","location":"Kabale - Mbarara Rd, Mbarara","tags":"","logo":"logos\/don.jpg","lat":"0.591607","lng":"30.6202272","user_id":"21","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":22,"brand_name":"Total","location":"Lake Resort Hotel Road, Mbarara","tags":"","logo":"","lat":"0.5914351","lng":"30.6200555","user_id":"22","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":23,"brand_name":"Shell","location":"Koranorya, Mbarara","tags":"","logo":"logos\/shell.png","lat":"0.5914351","lng":"30.6200555","user_id":"23","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":24,"brand_name":"Kobil","location":"Kampala Road, Mbarara","tags":"","logo":"logos\/don.jpg","lat":"0.5912632","lng":"30.6198838","user_id":"24","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":25,"brand_name":"Total","location":"Kampala Road, Mbarara","tags":"","logo":"logos\/total.png","lat":"0.5910913","lng":"30.6197121","user_id":"10","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":26,"brand_name":"Hared","location":"Soroti - Mbale Rd, Mbale","tags":"","logo":"logos\/don.jpg","lat":"1.0967538","lng":"34.1498796","user_id":"12","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":27,"brand_name":"Total","location":"Mbale","tags":"","logo":"logos\/total.png","lat":"1.0970967","lng":"34.1495362","user_id":"13","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":28,"brand_name":"Total","location":"Mbale","tags":"","logo":"logos\/total.png","lat":"1.0742503","lng":"34.1526264","user_id":"15","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":29,"brand_name":"Agrip","location":"Mbale","tags":"","logo":"logos\/don.jpg","lat":"1.0969252","lng":"34.1497079","user_id":"16","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":30,"brand_name":"Gapco","location":"Pallisa, Mbale","tags":"","logo":"logos\/gapco.png","lat":"1.0970109","lng":"34.1496221","user_id":"17","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":31,"brand_name":"Gapco","location":"Masindi","tags":"","logo":"logos\/gapco.png","lat":"1.6177354","lng":"31.5518409","user_id":"18","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":32,"brand_name":"Kobil","location":"Masindi","tags":"","logo":"logos\/don.jpg","lat":"1.6184121","lng":"31.5511536","user_id":"19","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":33,"brand_name":"Shell","location":"Bobi - Masindi Rd, Masindi","tags":"","logo":"logos\/shell.png","lat":"1.6197656","lng":"31.5497789","user_id":"20","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":34,"brand_name":"Fuelex","location":"Kampala - Gulu Highway","tags":"","logo":"logos\/don.jpg","lat":"1.6394727","lng":"31.6038387","user_id":"21","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":35,"brand_name":"Total","location":"Gulu","tags":"","logo":"logos\/total.png","lat":"2.3152418","lng":"31.8146973","user_id":"22","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":36,"brand_name":"Don Fuel","location":"Bobi - Masindi Rd, Kigumba","tags":"","logo":"logos\/don.jpg","lat":"1.639714","lng":"31.6036008","user_id":"23","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":37,"brand_name":"Moil","location":"Akokoro Road, Apac","tags":"","logo":"logos\/don.jpg","lat":"1.6420235","lng":"31.1807193","user_id":"24","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":38,"brand_name":"Kobil","location":"Chegere Road, Apac","tags":"","logo":"logos\/don.jpg","lat":"1.6446126","lng":"31.1779621","user_id":"5","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":39,"brand_name":"Gapco","location":"Lira - Gulu Rd, Lira","tags":"","logo":"logos\/gapco.png","lat":"1.6472015","lng":"31.1752048","user_id":"6","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":40,"brand_name":"Shell","location":"Gulu","tags":"","logo":"logos\/shell.png","lat":"2.3127195","lng":"31.8174525","user_id":"7","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":41,"brand_name":"Delta","location":"Gulu","tags":"","logo":"logos\/don.jpg","lat":"2.317764","lng":"31.811942","user_id":"8","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":42,"brand_name":"Gapco","location":"Lira - Gulu Rd, Lira","tags":"","logo":"logos\/gapco.png","lat":"1.6523789","lng":"31.1696847","user_id":"9","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":43,"brand_name":" Total","location":"Kitgum","tags":"","logo":"logos\/total.png","lat":"3.2990843","lng":"32.3186072","user_id":"10","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"},{"id":44,"brand_name":"Shell","location":"Gulu-Kitgum Rd, Kitgum","tags":"","logo":"logos\/shell.png","lat":"3.3039399","lng":"32.3130871","user_id":"11","created_at":"2016-04-17 10:50:09","updated_at":"2016-04-17 10:50:09"}]
        function initialize() {

            // Giving the map som options
            var mapOptions = {
                zoom: 8,
                center: new google.maps.LatLng(2.1291052,32.5729709)
            };

            // Creating the map
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);


            // Looping through all the entries from the JSON data
            for(var i = 0; i < json.length; i++) {

                // Current object
                var obj = json[i];

                // Adding a new marker for the object
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(obj.lat,obj.lng),
                    map: map,
                    description: obj.brand_name,
                    title: obj.brand_name// this works, giving the marker a title with the correct title
                });

                var infowindow = new google.maps.InfoWindow({
                    content: obj.brand_name
                });


                var desc = '<div>'+ obj.brand_name+'<br/>'+obj.location+'</div>'

                /* Adding a new info window for the object*/
                addClicker(marker, desc);
/*
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });*/






            } // end loop


            // Adding a new click event listener for the object
            function addClicker(marker, content) {
                google.maps.event.addListener(marker, 'click', function() {

                    if (infowindow) {infowindow.close();}
                    infowindow = new google.maps.InfoWindow({content: content});
                    infowindow.open(map, marker);

                });
            }










        }

        // Initialize the map
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection