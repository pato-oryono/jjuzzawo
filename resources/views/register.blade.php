@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
{{--        <div class="col-md-5 card">
            <h3>Login to Jjuzzawo.</h3>
            <form action="" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password">
                </div>

                <button class="btn btn-success">Login</button>
            </form>
        </div>--}}
        <div class="col-md-3"></div>
        <div class="col-md-6 card">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @elseif(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <h3>Want your Petrol Station listed?, Give us your details.</h3>
            <hr>
            <form action="" method="post">
                <input value="{{ csrf_token() }}" name="_token" type="hidden"/>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Firstname" name="firstname">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Lastname" name="lastname">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Phone" name="phone">
                </div>

                <button class="btn btn-success">Register</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>

@endsection