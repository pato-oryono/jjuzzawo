<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'fuel_prices';

    public function fuelType()
    {
        return $this->belongsTo('\App\FuelType', 'fuel_id');
    }

    public function station()
    {
        return $this->belongsTo('\App\Station', 'station_id');
    }
}
