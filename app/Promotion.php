<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'name',
        'description',
        'station_id',
        'start_date',
        'end_date'

    ];

    public function station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }
}
