<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    public function fuelType()
    {
        return $this->hasMany('\App\FuelType');
    }

    public function user()
    {
        return $this->belongsTo('\App\User', 'user_id');
    }
}
