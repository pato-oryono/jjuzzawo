<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

Route::get('/stations', function () {
    return view('stations');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/faqs', function () {
    return view('faqs');
});

Route::get('/', function () {
    return redirect('update');
});

Route::get('update',['middleware' => 'auth', function () {
    $services = \App\Service::all();
    $promotions = \App\Promotion::all();
    $stations = \App\Station::where('user_id', Auth::user()->id)->get();
    $types = \App\FuelType::all();

    $station_id = Auth::user()->station->id;

    //dd(Auth::user()->station);

    //dd($station_id);

    $prices = \App\Price::where('station_id', $station_id)->get();


    return view('update',
        [
            'stations' => $stations,
            'types'    =>$types,
            'prices'   => $prices,
            'services' => $services,
            'promotions'=>$promotions
        ]);
}]);

Route::post('update/{id}',['middleware' => 'auth', function ($id) {
    //$stations = \App\Station::where('user_id', Auth::user()->id)->get();
    //$types = \App\FuelType::all();
    $station_id = Auth::user()->station->id;

    $prices = \App\Price::where('station_id', $station_id)
        ->Where('fuel_id',$id)->get();


    $input = Input::all();

    if(Input::has('petroleum')){
        $validity = Validator::make($input, array(
            'petroleum' => 'Required|Min:4|Max:6',

        ));

        if ($validity->fails()) {
            return Redirect::to('update')->withErrors($validity)->with('error', 'something went wrong');
        } else {
            \App\Price::where('station_id', $station_id)
                ->Where('fuel_id',$id)->update(array(
                    'price' => $input['petroleum'],
                ));

            return Redirect::to('update')->with('success', "Update Successful.");
        }


    }elseif(Input::has('diesel')){
        $validity = Validator::make($input, array(
            'diesel' => 'Required|Min:4|Max:6',

        ));

        if ($validity->fails()) {
            return Redirect::back()->withErrors($validity)->with('error', 'something went wrong');
        } else {
            \App\Price::where('station_id', $station_id)
                ->Where('fuel_id',$id)->update(array(
                    'price' => $input['diesel'],
                ));

            return Redirect::to('update')->with('success', "Update Successful.");
        }

    }elseif(Input::has('kerosene')){
        $validity = Validator::make($input, array(
            'kerosene' => 'Required|Min:4|Max:6',

        ));

        if ($validity->fails()) {
            return Redirect::back()->withErrors($validity)->with('error', 'something went wrong');
        } else {
            \App\Price::where('station_id', $station_id)
                ->Where('fuel_id',$id)->update(array(
                    'price' => $input['kerosene'],
                ));

            return Redirect::to('update')->with('success', "Update Successful.");
        }

    }elseif(Input::has('gas')){
        $validity = Validator::make($input, array(
            'gas' => 'Required|Min:4|Max:5',

        ));

        if ($validity->fails()) {
            return Redirect::back()->withErrors($validity)->with('error', 'something went wrong');
        } else {
            \App\Price::where('station_id', $station_id)
                ->Where('fuel_id',$id)->update(array(
                    'price' => $input['gas'],
                ));

            return Redirect::to('update')->with('success', "Update Successful.");
        }

    }





}]);



Route::get('/auth/login', function () {
    Auth::logout();
    return view('login');
});

Route::get('/auth/register', function () {
    return view('register');
});

Route::post('/auth/register', function () {
    $input = Input::all();
    $validity = Validator::make($input, array(
        'firstname' => 'Required|Min:3|Max:80|Alpha',
        'lastname' => 'Required|Min:3|Max:80|Alpha',
        'email' => 'Required|Min:3|Unique:prospects',
        'phone' => array('required', 'regex:/^((0)|(256)|(\+256))[1-9]\d{8}$/', 'Unique:prospects')
    ));

    if ($validity->fails()) {
        return Redirect::to('/auth/register')->withErrors($validity);
    } else {
        \App\Prospect::create(array(
            'firstname' => $input['firstname'],
            'lastname' => $input['lastname'],
            'email'=>$input['email'],
            'phone'=> $input['phone']
        ));

        return Redirect::to('/auth/register')->with('success', "Registration successful.");
    }
});



Route::post('/auth/login', function () {
    $credentials = [
        'email'     => Input::get('email'),
        'password'  => Input::get('password')
    ];

    $remember = Input::get('remember');

    return Auth::attempt($credentials, $remember) ? redirect::intended('/update') : redirect::to('/auth/login')->with('error', 'Invalid username or password');
});

Route::get('/download', function () {
   return view('download');
});

Route::get('/promotions/add', function(){
   return view('add_promotions');
});

Route::post('/promotions/add', function(){
    $input = Input::all();
    $validity = Validator::make($input, array(
        'promotion_name' => 'Required|Min:3|Max:80|Alpha',
        'description' => 'Required|Min:3|Max:80',
        'start_date' => 'Required',
        'end_date' => 'Required'
    ));

    if ($validity->fails()) {
        return Redirect::to('/promotions/add')->withErrors($validity);
    } else {
        \App\Promotion::create(array(
            'name' => $input['promotion_name'],
            'station_id'=>Auth::user()->station->id,
            'description' => $input['description'],
            'start_date'=>$input['start_date'],
            'end_date'=> $input['end_date']
        ));

        return Redirect::to('/promotions/add')->with('success', "Registration successful.");
    }
});

Route::get('/services/add', function(){
    return view('add_services');
});

Route::post('/services/add', function(){
    $input = Input::all();
    $validity = Validator::make($input, array(
        'service_name' => 'Required|Min:3|Max:80|Alpha',
        'description' => 'Required|Min:3|Max:80',
    ));

    if ($validity->fails()) {
        return Redirect::to('/services/add')->withErrors($validity);
    } else {
        \App\Service::create(array(
            'name' => $input['service_name'],
            'station_id'=>Auth::user()->station->id,
            'description' => $input['description'],
        ));

        return Redirect::to('/services/add')->with('success', "Registration successful.");
    }
});
Route::get('/services/{id}/delete/',function($id){
    $selected = App\Service::find($id);
    $selected->delete();
    return Redirect::to('/update')->with('success', 'Successfully deleted.');
});

Route::get('/promotions/{id}/delete/',function($id){
    $selected = App\Promotion::find($id);
    $selected->delete();
    return Redirect::to('/update')->with('success', 'Successfully deleted.');
});






